<?php
namespace app\module\student;
class Student{

    public $name="";

    public function sayPondit(){
        return "Hello from  ".$this->name." I'm from Student Module";
    }

    public function __construct($name)
    {
        $this->name=$name;
    }
}